import { getMailer } from '@themost/mailer';
import { DataError } from '@themost/data';
import { TraceUtils } from "@themost/common";
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === 2) {
        const context = event.model.context;
        /**
         * @type {import('@themost/mailer').MailerHelper}
         */
        const mailer = getMailer(context);

        const currentCardStatus = await context
            .model('StudentDiningCard')
            .where('id')
            .equal(event.target.id)
            .select('active')
            .value();
        let previousCardStatus = 'UnknownStatus';
        if (event.previous) {
            previousCardStatus = event.previous.active;
        } else {
            throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudentDiningCard');
        }
        if (currentCardStatus === previousCardStatus) {
            return;
        }

        // get mail template
        const mailTemplate = await context
            .model('MailConfigurations')
            .where('target')
            .equal('DiningCardActiveStatusChange')
            .silent()
            .getItem();
        if (mailTemplate == null) {
            //
            return;
        }

        //get parent action data
        let item;
        if (typeof event.target.action === 'object' && typeof event.target.action.id === 'number' && Number.isInteger(event.target.action.id) && event.target.action.id > 0) {
            item = await context
                .model('DiningRequestAction')
                .where('id')
                .equal(event.target.action.id)
                .expand({
                    name: 'student',
                    options: {
                        $expand: 'person'
                    }
                })
                .silent()
                .getItem();

        } else if (typeof event.target.action === 'number' && Number.isInteger(event.target.action) && event.target.action > 0) {
            item = await context
                .model('DiningRequestAction')
                .where('id')
                .equal(event.target.action)
                .expand({
                    name: 'student',
                    options: {
                        $expand: 'person'
                    }
                })
                .silent()
                .getItem();
        }

        const application = await context
            .model('WebApplication')
            .where('alternateName')
            .equal('students')
            .and('applicationSuite')
            .equal('universis')
            .silent()
            .getItem();

        const config = await context
            .model('StudentRequestConfiguration')
            .where('additionalType')
            .equal('DiningRequestAction')
            .and('inLanguage')
            .equal(event.target.inLanguage || context.locale)
            .silent()
            .getItem();

        if (item && application && config) {

            const email = item.student.person.email;

            if (email == null || typeof email !== 'string') {
                TraceUtils.warn(`Cannot send email for ${item.student.person.familyName} ${item.student.person.givenName}. Student [${item.student.id}] email does not exist.`);
                return;
            }
            await new Promise((resolve) => {
                mailer.template(mailTemplate.template)
                    .subject(mailTemplate.subject)
                    .bcc(mailTemplate.bcc || '')
                    .to(email)
                    .send({
                        model: item,
                        application: application,
                        configuration: config
                    }, (err) => {
                        if (err) {
                            try {
                                TraceUtils.error('An error occurred while trying to send an email notification after status change.');
                                TraceUtils.error(`Student=${item.student.id}, template=${mailTemplate.template}`);
                                TraceUtils.error(err);
                            } catch (err1) {
                                // do nothing
                            }
                            return resolve();
                        }

                        return resolve();
                    })
            });

        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
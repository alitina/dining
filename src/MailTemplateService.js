import { ApplicationService } from "@themost/common";
import { getMailer } from "@themost/mailer";
import path from 'path';

/**
 * @interface ExtraTemplateConfiguration
 * @property {string} name
 * @property {string} path
 */


/**
 *
 */
class MailTemplateService extends ApplicationService {
    constructor(app) {
        super(app);
        /**
         * @type {Array<ExtraTemplateConfiguration>}
         */
        this.extraTemplates = [];
        this.install();
    }
    install() {
        const MailerHelperPrototype = Object.getPrototypeOf(getMailer(null));
        const thisService = this;
        Object.defineProperty(MailerHelperPrototype, 'getTemplatePath', {
            configurable: true,
            enumerable: true,
            get() {
                return function (template, extension) {
                    const extraTemplate = thisService.extraTemplates.find((item) => {
                        return item.name === template;
                    });
                    if (extraTemplate != null) {
                        return extraTemplate.path;
                    }
                    // get execution path
                    const executionPath = thisService.getApplication().getConfiguration().getExecutionPath();
                    return path.resolve(executionPath, `./templates/mails/${template}/html.${extension}`);
                }
            }
        });
    }
}

export {
    MailTemplateService
}
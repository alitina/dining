import { DataError, HttpForbiddenError } from "@themost/common";
import { EdmMapping, EdmType } from "@themost/data";
import EnableAttachmentModel from './EnableAttachmentModel';

class DiningRequestAction extends EnableAttachmentModel {
    constructor() {
        super();
    }
    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    @EdmMapping.param('extraAttributes', 'Object', true, true)
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('AddAttachment', 'Attachment')
    async addAttachment(file, extraAttributes) {
        const attachment = Object.assign({
            name: file.contentFileName,
            originalname: file.contentFileName
        }, file, extraAttributes);
        return await super.addAttachment(attachment);
    }
    /**
     * Removes an attachment
     * @param {*} attachment
     */
    @EdmMapping.param('attachment', 'Attachment', true, true)
    @EdmMapping.action('RemoveAttachment', 'Attachment')
    async removeAttachment(attachment) {
        return await super.removeAttachment(attachment.id);
    }

    /**
     * Rejects an attachment
     * @param {*} attachment
     */
    @EdmMapping.param('attachment', 'Attachment', true, true)
    @EdmMapping.action('RejectAttachment', 'Attachment')
    async rejectAttachment(attachment) {
        return await super.rejectAttachment(attachment.id);
    }    
    
    /**
     * Reverts (re-accepts) an attachment
     * @param {*} attachment
     */
     @EdmMapping.param('attachment', 'Attachment', true, true)
     @EdmMapping.action('RevertAttachment', 'Attachment')
     async revertAttachment(attachment) {
         return await super.revertAttachment(attachment.id);
     }

    /**
     * Add action messages
     * @param {Array<any>} items
     */
 @EdmMapping.param('item', 'DiningRequestActionMessage', true, true)
 @EdmMapping.action('messages', 'DiningRequestActionMessage')
 async postMessages(item) {
     // set initiator
     item.action = this.getId();
     return this.context.model('DiningRequestActionMessage').save(item);
 }

 /**
  * Gets item review
  */
 @EdmMapping.func('review', 'DiningRequestActionReview')
 getReview() {
     return this.context.model('DiningRequestActionReview')
         .where('itemReviewed').equal(this.getId()).prepare();
 }

 /**
  * Set item review
  * @param {*} item
  */
 @EdmMapping.param('item', 'DiningRequestActionReview', true, true)
 @EdmMapping.action('review', 'DiningRequestActionReview')
 async setReview(item) {
     const DiningRequestActionReviews = this.context.model('DiningRequestActionReview');
     // infer object state
     const currentReview = await DiningRequestActionReviews.where('itemReviewed').equal(this.getId()).getItem();
     if (currentReview == null) {
         if (item == null) {
             return;
         }
         // a new item is going to be inserted
         delete item.id;
         // set reviewed item
         item.itemReviewed = this.getId();
     } else {
         if (item == null) {
             // delete review
             DiningRequestActionReviews.remove(currentReview);
         }
         // up
         item.id = currentReview.id;
         // set reviewed item
         item.itemReviewed = this.getId();
     }
     return DiningRequestActionReviews.save(item);
 }

 /**
  * Adds an attachment
  * @param {{contentFileName:string}|*} attachment
  * @param {*=} message
  */
 @EdmMapping.param('message', 'Object', true, true)
 @EdmMapping.param('attachment', EdmType.EdmStream, true)
 @EdmMapping.action('SendMessage', 'DiningRequestActionMessage')
 sendMessage(attachment, message) {
     const thisArg = this;
     return new Promise((resolve, reject) => {
         this.context.db.executeInTransaction((cb) => {
                (async () => {
                    const DiningRequestActionMessage = thisArg.context.model('DiningRequestActionMessage');
                    if (message.recipient) {
                        // try to find the message recipient by bypassing privileges
                        // note: Students do not-and should not-have read access to Account or User models.
                        // this process will help to identify an account by its name, e.g. recipient: 'DiningUsers'
                        const recipient = await thisArg.context.model('Account')
                            .where('name').equal(message.recipient.name || message.recipient)
                            .select('id')
                            .silent()
                            .value()
                        ?? await thisArg.context.model('Account')
                            .where('id').equal(message.recipient.id || message.recipient)
                            .select('id')
                            .silent()
                            .value()
                        if (recipient == null) {
                            throw new DataError('The specified message recipient (User/Account) cannot be found.');
                        }
                        // and override the message recipient
                        message.recipient = recipient;
                    }
                    const newMessage = DiningRequestActionMessage.convert(message);
                    Object.assign(newMessage, {
                        action: thisArg.getId()
                    });
                    // add message
                    await DiningRequestActionMessage.silent().save(newMessage);
                    // if message has an attachment
                    if (attachment) {
                        // prepare attachment data
                        const newAttachment = Object.assign({
                            name: attachment.contentFileName,
                            originalname: attachment.contentFileName
                        }, attachment);
                        // and add attachment
                        await newMessage.addAttachment(newAttachment)
                    }
                })().then(() => {
                    return cb();
                }).catch((err) => {
                    return cb(err);
                });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve(message);
            })
        });
    }  

  
//remove dining request action
@EdmMapping.action('Cancel', 'DiningRequestAction')
async cancel() {
    /**
    * @type {import('@themost/data').DataModel}
    */

    const model = this.getModel();

    const item = await model.where('owner/name')
        .equal(this.context.user.name)
        .and('actionStatus/alternateName')
        .equal('PotentialActionStatus')
        .and('id')
        .equal(this.getId())
        .getItem();

    if (item == null) {
        throw new HttpForbiddenError();
    }

    return model.silent().remove(item);
}

}

module.exports = DiningRequestAction;
